package com.task.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskResponseDTO implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3186651183119262609L;
	
	Integer code;
	String description;

}
