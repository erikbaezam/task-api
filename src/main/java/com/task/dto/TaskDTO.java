package com.task.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2106031534681197545L;
	
	Integer id;
	String description;
	Date creationDate;
	Boolean valid;

}
