package com.task.service;

import java.util.List;

import com.task.dto.TaskDTO;
import com.task.dto.TaskResponseDTO;


public interface TaskService {

	TaskResponseDTO saveTask(TaskDTO taskDTO);

	List<TaskDTO> getAllTask();

	TaskResponseDTO deleteTaks(Integer id);

	TaskResponseDTO editTask(TaskDTO taskDTO);

}
