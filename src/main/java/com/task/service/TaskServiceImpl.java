package com.task.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.task.dto.TaskDTO;
import com.task.dto.TaskResponseDTO;
import com.task.entity.TaskEntity;
import com.task.repository.TaskRepository;

@Service
@Transactional
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskRepository taskRepository;

	private final DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();

	@Override
	public TaskResponseDTO saveTask(TaskDTO taskDTO) {
		TaskResponseDTO taskResponseDTO = TaskResponseDTO.builder().build();
		taskDTO.setCreationDate(new Date());
		taskDTO.setValid(Boolean.TRUE);

		try {
			taskRepository.save(dozerBeanMapper
					.map(taskDTO, TaskEntity.class));
			taskResponseDTO.setCode(1);
			taskResponseDTO.setDescription("Success");
		} catch (Exception e) {
			taskResponseDTO.setCode(0);
			taskResponseDTO.setDescription("Failure" + e.getMessage());
		}

		return taskResponseDTO;
	}

	@Override
	public List<TaskDTO> getAllTask() {
		return taskRepository.findAll().stream().map(t -> dozerBeanMapper.map(t, TaskDTO.class))
				.collect(Collectors.toList());
	}

	@Override
	public TaskResponseDTO deleteTaks(Integer id) {
		TaskResponseDTO taskResponseDTO = TaskResponseDTO.builder().build();

		try {

			taskRepository.deleteById(id);
			taskResponseDTO.setCode(1);
			taskResponseDTO.setDescription("Success");
		} catch (Exception e) {
			taskResponseDTO.setCode(0);
			taskResponseDTO.setDescription("Failure" + e.getMessage());
		}
		return taskResponseDTO;
	}

	@Override
	public TaskResponseDTO editTask(TaskDTO taskDTO) {
		TaskResponseDTO taskResponseDTO = TaskResponseDTO.builder().build();
		try {
			taskRepository.save(dozerBeanMapper.map(taskDTO, TaskEntity.class));
			taskResponseDTO.setCode(1);
			taskResponseDTO.setDescription("Success");
		} catch (Exception e) {
			taskResponseDTO.setCode(0);
			taskResponseDTO.setDescription("Failure" + e.getMessage());
		}

		return taskResponseDTO;
	}

}
