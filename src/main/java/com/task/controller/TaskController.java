package com.task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task.dto.TaskDTO;
import com.task.dto.TaskResponseDTO;
import com.task.service.TaskService;

@RestController
public class TaskController {

	@Autowired
	private TaskService taskService;

	@PostMapping(value = "/save-task", produces = MediaType.APPLICATION_JSON_VALUE)
	public TaskResponseDTO saveTask(@RequestBody TaskDTO taskDTO) {
		return taskService.saveTask(taskDTO);
	}

	@PostMapping(value = "/edit-task", produces = MediaType.APPLICATION_JSON_VALUE)
	public TaskResponseDTO editTask(@RequestBody TaskDTO taskDTO) {
		return taskService.editTask(taskDTO);
	}

	@DeleteMapping(value = "/delete-task", produces = MediaType.APPLICATION_JSON_VALUE)
	public TaskResponseDTO deleteTask(@RequestParam Integer id) {
		return taskService.deleteTaks(id);
	}

	@GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TaskDTO> getAllTask() {
		return taskService.getAllTask();
	}

}
