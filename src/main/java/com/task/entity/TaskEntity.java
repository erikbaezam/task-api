package com.task.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Table(name = "task")
public class TaskEntity {

	@Id
	@GeneratedValue(generator="task_sequence",strategy=GenerationType.AUTO)
	Integer id;

	@Column(name = "description")
	String description;

	@Column(name = "creation_date")
	Date creationDate;

	@Column(name = "valid")
	Boolean valid;

}
